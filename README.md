# API Reclutador de mutantes de Magneto

Ejercicio práctico de aplicativo REST de detección de ADN mutante.
El ejercicio hace uso del algoritmo de detección de ADN desarrollado previamente en forma de librería.-

El aplicativo se desarrolló en Spring Boot, haciendo provecho de su autoconfiguración. 
En la rama master, se hace uso de base de datos en memoria H2. Adicionalmente, se tiene otra rama con una versión utilizando POSTGRESQL.

El aplicativo esta configurado para ser desplegado en la nube de Heroku así como también para correrse localmente o desplegarse en otros contenedores.

Para mejorar la performance, el API ejecuta el algoritmo de detección y de forma asíncrona (hilo aparte) persiste en la base de datos la información de la detección realiazada. De modo de responder inmediatamente luego de correr el algoritmo.

La nube de Heroku, permite escalabilidad con configuración manuel o automática según el plan.

Se desplegó también como función lambda de AWS por su eficiencia y escalabilidad automática. En este caso, a modo de prueba de concepto, solo se desplegó el algoritmo de detección y no el API completo con estadísticas.

### Instalación local

#### Pre-requisitos

Para el funcionamiento del proyecto, es necesario contar con Maven y Java instalado. Maven te servirá para compilar el codigo fuente y generar el ejecutable.
También es necesario tener permiso sobre el repositorio.

La aplicación API hace uso de la librería "mutant-recruiter". Esta misma se puede encontrar en el repositorio: git clone https://gitlab.com/adrianprause/mutantrecruiter-api-springboot.git

Para instalar el API, es necesario contar primero con la librería del reclutador dentro del repositorio de maven.

#### Comenzando

Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas.

Posicionate sobre una carpeta donde descargar el código fuente y en la consola escribe:

```
git clone https://gitlab.com/adrianprause/mutantrecruiter.git
git clone https://gitlab.com/adrianprause/mutantrecruiter-api-springboot.git
```

#### Pasos de instalación

Para compilar el proyecto sigue los siguientes pasos:
Posicionate sobre la carpeta que contiene el código fuente de "mutantrecruiter" y en la consola escribe:

```
mvn install
```

Este comando instalará el reclutador como librería en el repositorio de maven. Este mismo es usado como dependencia en el proyecto api.

Posicionate sobre la carpeta que contiene el código fuente de "mutantrecruiter-api-springboot" y en la consola escribe:

```
mvn package
```

Si todo sale bien, debería haberte ejecutado las pruebas y decirte: BUILD SUCCESS

#### Ejecutando las pruebas️

Para ejecutar las pruebas unitarias del proyecto sigue los siguientes pasos:
Posicionate sobre la carpeta que contiene el código fuente y en la consola escribe:

```
mvn test
```

#### Cobertura de las pruebas️

Para la cobertura de test, se usó Jacoco como plugin de maven. Luego de correr "mvn test" anteriormente, dentro de target/site/jacoco se puede ver los resultados de las mediciones de cobertura:

```
Total	 63 of 380	 > 83%
```

Jococo crea un reporte html donde se puede ver toda la información de forma gráfica:

```
open target/site/jacoco/index.html
```


#### Uso de la herramienta

Para ejecutar el detector de ADN mutante como un API sigue los siguientes pasos:

Posicionate sobre la carpeta que contiene el código fuente y en la consola escribe:

```
mvn spring-boot:run
```

Si todo sale bien, debería levantar automaticamente la aplicación con SpringBoot y estar disponible el servicio API en el puerto 8080 del localhost.

Dentro del endpoint: http://localhost:8080/api/v2/api-docs se puede ver documentación sobre los endpoints y su uso.

#### Ejemplo detección de ADN

El API cuenta con un endpoint de tipo POST a la url http://localhost:8080/api/mutant donde recibe dentro del cuerpo de la petición en formato json como el ejemplo a continuación:

```
{
	"dna": ["ATGCGA","CAGTGC","TTATGT","AGAAGG","CCCCTA","TCACTG"]
}
```

Como respuesta, puede retornar cabecera con estado 200 cuando se trata de ADN mutante o 403 cuando se trata de una ADN humano.

El API cuenta con otro endpoint que permite ver las estadísticas de detecciones realizadas. En este caso se puede acceder con una request de tipo GET a la URI: http://localhost:8080/api/stats

Esta retorna un JSON con 3 propiedades: cantidad de detecciones mutantes, cantidad de detecciones humanas, y la relación mutantes sobre humanos.

```
{
    "ratio": 3,
    "count_mutant_dna": 3,
    "count_human_dna": 1
}
```

## Uso de la herramienta online

Este aplicativo se encuentra desplegado en la nube de Heroku, bajo la URL:
http://mutantrecruiter-springboot-api.herokuapp.com/

- [POST] http://mutantrecruiter-springboot-api.herokuapp.com/mutant
- [GET] http://mutantrecruiter-springboot-api.herokuapp.com/stats

Adicionalmente, el endpoint de detección se encuentra desplegado como función lambda de AWS en la URL:

- [POST]  https://149te3iw96.execute-api.us-east-2.amazonaws.com/prod

## PostMan collections

Los siguientes links contienen colecciones de postman para realizar pruebas:

- [Aws] https://www.getpostman.com/collections/70ef0be76a7e4df02e0b
- [Heroku] https://www.getpostman.com/collections/ee8612098d7c02c0692d

## Pruebas de fuerza

Para la realización de pruebas de fuerza se usó una herramienta online llamada Load Impact. Las pruebas se realizaron de la misma manera para Heroku y Aws Lambda. Las pruebas consisten en peticiones simultaneas de hasta 50 usuarios a lo largo de 10 segundos. Las pruebas están condicionadas a las limitaciones del plan gratuito de Load Impact.

El siguiente link contiene el reporte para Heroku:
- https://app.loadimpact.com/k6/anonymous/29b1fa23badb4d0693b3fc7e96806f94

El siguiente link contiene el reporte para Aws Lambda:
- https://app.loadimpact.com/k6/anonymous/746c2db78ed540d3bc28535568b73300


## Autores️


* **Adrián Prause** - *Trabajo Inicial* - (https://gitlab.com/adrianprause)
