package magneto.backend.repository.entities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import magneto.backend.model.entities.DnaLog;

@Repository
@Transactional
public interface DnaLogRepository extends JpaRepository<DnaLog, Long>, JpaSpecificationExecutor<DnaLog> {

	Long countByMutant(boolean isMutant);

}
