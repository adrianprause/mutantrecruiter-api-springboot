package magneto.backend.service.stats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import magneto.backend.exception.ServiceException;
import magneto.backend.repository.entities.DnaLogRepository;
import magneto.backend.service.common.BaseService;
import magneto.rest.api.stats.StatsDto;

@Service
public class StatsService extends BaseService {

	@Autowired
	private DnaLogRepository dnaLogRepository;

	public StatsDto getStats() throws ServiceException {

		StatsDto stats = new StatsDto();
		stats.setCountMutantDna(dnaLogRepository.countByMutant(true));
		stats.setCountHumanDna(dnaLogRepository.countByMutant(false));
		if (stats.getCountHumanDna() > 0) {
			stats.setRatio((float) stats.getCountMutantDna() / stats.getCountHumanDna());
		} else {
			stats.setRatio((float) stats.getCountMutantDna() / 1);
		}
		return stats;

	}

}
