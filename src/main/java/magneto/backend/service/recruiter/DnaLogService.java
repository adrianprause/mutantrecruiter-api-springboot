package magneto.backend.service.recruiter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import magneto.backend.model.entities.DnaLog;
import magneto.backend.repository.entities.DnaLogRepository;
import magneto.backend.service.common.BaseService;

@Service
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class DnaLogService extends BaseService {

	@Autowired
	private DnaLogRepository dnaLogRepository;

	@Async
	public void save(DnaLog log) {

		dnaLogRepository.save(log);

	}

	@Async
	public void deleteAll() {

		dnaLogRepository.deleteAll();

	}

}
