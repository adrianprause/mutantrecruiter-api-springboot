package magneto.backend.service.recruiter;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import magneto.MutantRecruiter;
import magneto.backend.exception.ServiceException;
import magneto.backend.model.entities.DnaLog;
import magneto.backend.service.common.BaseService;

@Service
public class RecruiterService extends BaseService {

	@Autowired
	private DnaLogService dnaLogService;
	
	@Value("${recruiter-debug}")
	private boolean debug;

	public void run(String[] dna) throws ServiceException {

		// run recruiter
		boolean isMutant = MutantRecruiter.isMutant(dna, debug);

		// store log
		DnaLog log = new DnaLog();
		log.setDna(Arrays.toString(dna));
		log.setMutant(isMutant);
		save(log);

		if (!isMutant) {
			throw new ServiceException("Non mutant DNA");
		}

	}

	public void save(DnaLog log) {

		dnaLogService.save(log);

	}

}
