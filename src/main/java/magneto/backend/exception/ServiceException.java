package magneto.backend.exception;

/**
 * Backend Service Layer Exception
 * 
 * @author adrian.prause
 *
 */
public class ServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7649138572846818907L;

	public ServiceException(String message) {
		super(message);
	}

}