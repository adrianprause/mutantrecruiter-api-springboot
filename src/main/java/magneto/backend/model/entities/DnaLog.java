package magneto.backend.model.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.SequenceGenerator;

@Entity
public class DnaLog implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 4126724030194782099L;

	@Id
	@SequenceGenerator(name = "dna_log_generator", sequenceName = "dna_log_sequence", initialValue = 1)
	@GeneratedValue(generator = "dna_log_generator")
	private Long id;

	@Lob
	@Column(nullable = false)
	private String dna;

	private boolean mutant;

	public DnaLog() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDna() {
		return dna;
	}

	public void setDna(String dna) {
		this.dna = dna;
	}

	public boolean isMutant() {
		return mutant;
	}

	public void setMutant(boolean mutant) {
		this.mutant = mutant;
	}

}
