package magneto.rest.api.recruiter;

import org.hibernate.validator.constraints.NotEmpty;

import magneto.rest.api.common.dto.BaseDto;

public class DnaDto extends BaseDto {

    @NotEmpty(message = "Dna is mandatory")
	private String[] dna;

	public String[] getDna() {
		return dna;
	}

	public void setDna(String[] dna) {
		this.dna = dna;
	}

}
