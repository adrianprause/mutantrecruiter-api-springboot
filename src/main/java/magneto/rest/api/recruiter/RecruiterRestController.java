package magneto.rest.api.recruiter;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import magneto.backend.exception.ServiceException;
import magneto.backend.service.recruiter.DnaLogService;
import magneto.backend.service.recruiter.RecruiterService;
import magneto.rest.api.common.controller.BaseRestController;

@RestController
@RequestMapping("/mutant")
public class RecruiterRestController extends BaseRestController {

	@Autowired
	private RecruiterService recruiterService;
	
	@Autowired
	private DnaLogService dnaLogService;

	@PostMapping
	@ResponseStatus(value = HttpStatus.OK)
	public void mutant(@Valid @RequestBody DnaDto dnaDto) throws ServiceException {
		
		recruiterService.run(dnaDto.getDna());
		
	}
	
	@GetMapping("/drop")
	@ResponseStatus(value = HttpStatus.OK)
	public void deleteAll() throws ServiceException {
		
		dnaLogService.deleteAll();
		
	}

}
