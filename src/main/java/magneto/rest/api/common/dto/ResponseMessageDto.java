package magneto.rest.api.common.dto;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.FieldError;

/**
 * DTO for common response messages
 * 
 * @author adrian.prause
 *
 */
public class ResponseMessageDto {

	private String message;
	private String code;
	private List<String> detail = new ArrayList<String>();
	private FieldError[] fieldErrors;

	public ResponseMessageDto(String code, String message) {
		this.message = message;
		this.code = code;
	}

	public ResponseMessageDto(FieldError[] fieldErrors) {
		this.message = "Request not valid";
		if (fieldErrors != null) {
			for (FieldError error : fieldErrors) {
				this.detail.add(error.getField() + ": " + error.getDefaultMessage());
			}
		}
		this.fieldErrors = fieldErrors;
	}

	public String getMessage() {
		return message;
	}

	public String getCode() {
		return code;
	}

	public List<String> getDetail() {
		return detail;
	}

	public FieldError[] getFieldErrors() {
		return fieldErrors;
	}

}