package magneto.rest.api.common.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import magneto.backend.exception.ServiceException;
import magneto.rest.api.common.dto.ResponseMessageDto;

/**
 * Base REST controller every controller should extends of
 * 
 * @author adrian.prause
 *
 */
@Component
public class BaseRestController {

	static Logger log = Logger.getLogger(BaseRestController.class);

	@ExceptionHandler(ServiceException.class)
	@ResponseStatus(HttpStatus.FORBIDDEN)
	public void serviceHandler(ServiceException ex) {
		log.warn("ServiceException " + ex.getMessage());
	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseMessageDto> serviceHandler(Exception ex) {
		log.error("Exception " + ex.getMessage());
		ex.printStackTrace();
		ResponseMessageDto messageDTO = new ResponseMessageDto(HttpStatus.INTERNAL_SERVER_ERROR.name(),
				HttpStatus.INTERNAL_SERVER_ERROR.name());
		return new ResponseEntity<ResponseMessageDto>(messageDTO, HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(MethodArgumentNotValidException.class)
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ResponseBody
	public ResponseMessageDto validationError(MethodArgumentNotValidException ex) {
		BindingResult result = ex.getBindingResult();
		final List<FieldError> fieldErrors = result.getFieldErrors();
		return new ResponseMessageDto((FieldError[]) (fieldErrors.toArray(new FieldError[fieldErrors.size()])));
	}

}
