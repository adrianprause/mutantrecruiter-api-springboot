package magneto.rest.api.stats;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import magneto.backend.exception.ServiceException;
import magneto.backend.service.stats.StatsService;
import magneto.rest.api.common.controller.BaseRestController;

@RestController
@RequestMapping("/stats")
public class StatsRestController extends BaseRestController {

	@Autowired
	private StatsService statsService;

	@GetMapping
	@ResponseStatus(value = HttpStatus.OK)
	public StatsDto mutant() throws ServiceException {

		return statsService.getStats();

	}

}
