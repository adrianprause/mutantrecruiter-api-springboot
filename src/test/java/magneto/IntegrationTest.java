package magneto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import magneto.rest.api.stats.StatsDto;
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class IntegrationTest extends AbstractTest {

	private static String uri = "/stats";

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void test_1_200_response() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());

	}

	@Test
	public void test_2_empty_stats() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());

		String content = mvcResult.getResponse().getContentAsString();
		StatsDto stats = super.mapFromJson(content, StatsDto.class);
		assertEquals(0, stats.getCountHumanDna());
		assertEquals(0, stats.getCountMutantDna());
		assertTrue(stats.getRatio() == 0);

	}

	@Test
	public void test_3_inserts() throws Exception {

		int humansCount = 100;
		int mutantsCount = 40;
		
		postHumanDna(humansCount);
		postMutantDna(mutantsCount);

	}

	@Test
	public void test_4_stats() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());
		
		int humansCount = 100;
		int mutantsCount = 40;
		
		postHumanDna(humansCount);
		postMutantDna(mutantsCount);
		
		String content = mvcResult.getResponse().getContentAsString();
		StatsDto stats = super.mapFromJson(content, StatsDto.class);
		assertEquals(humansCount, stats.getCountHumanDna());
		assertEquals(mutantsCount, stats.getCountMutantDna());
		assertEquals("0.4", String.valueOf(stats.getRatio()));

	}

	private void postHumanDna(int count) throws Exception {

		for (int i = 0; i < count; i++) {

			MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/mutant").contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(getHumanExampleJsonInputString())).andReturn();
			assertEquals(403, mvcResult.getResponse().getStatus());

		}

	}
	
	private void postMutantDna(int count) throws Exception {

		for (int i = 0; i < count; i++) {

			MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post("/mutant").contentType(MediaType.APPLICATION_JSON_VALUE)
					.content(getMutantExampleJsonInputString())).andReturn();
			assertEquals(200, mvcResult.getResponse().getStatus());

		}

	}

}
