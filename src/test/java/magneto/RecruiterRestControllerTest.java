package magneto;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class RecruiterRestControllerTest extends AbstractTest {

	private static String uri = "/mutant";
	
	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void test_invalid_input_dna() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("invalid input")).andReturn();

		assertEquals(500, mvcResult.getResponse().getStatus());

	}

	@Test
	public void test_empty_input_dna() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content("{\"dna\":[]}")).andReturn();

		assertEquals(400, mvcResult.getResponse().getStatus());

	}

	@Test
	public void test_mutant_dna() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(getMutantExampleJsonInputString())).andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());

	}

	@Test
	public void test_human_dna() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE)
				.content(getHumanExampleJsonInputString())).andReturn();

		assertEquals(403, mvcResult.getResponse().getStatus());

	}

}
