package magneto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import magneto.rest.api.stats.StatsDto;

public class StatsRestControllerTest extends AbstractTest {

	private static String uri = "/stats";

	@Override
	@Before
	public void setUp() {
		super.setUp();
	}

	@Test
	public void test_200_response() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());

	}

	@Test
	public void test_empty_stats() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());

		String content = mvcResult.getResponse().getContentAsString();
		StatsDto stats = super.mapFromJson(content, StatsDto.class);
		assertEquals(0, stats.getCountHumanDna());
		assertEquals(0, stats.getCountMutantDna());
		assertTrue(stats.getRatio() == 0);

	}

	@Test
	@Sql("/dna_log.sql")
	public void test_stats() throws Exception {

		MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(uri).accept(MediaType.APPLICATION_JSON_VALUE))
				.andReturn();

		assertEquals(200, mvcResult.getResponse().getStatus());
		
		String content = mvcResult.getResponse().getContentAsString();
		StatsDto stats = super.mapFromJson(content, StatsDto.class);
		assertEquals(100, stats.getCountHumanDna());
		assertEquals(40, stats.getCountMutantDna());
		assertEquals("0.4", String.valueOf(stats.getRatio()));
	}

}
